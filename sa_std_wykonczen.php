<?php
/** Template Name: Standardy wykończeń bloki
**/
?>
<?php get_header(); ?>


<!-- <div style="background-image:url(<?php the_field('bg_top_subpage') ?>);" class="breadcrumb-bg"> -->

<div style="background-image:url(https://sensowni.pl/wp-content/uploads/2020/01/IMG_2558.jpg);" class="breadcrumb-bg">
    <div class="container">
        <!-- <nav class="breadcrumb d-flex align-items-center" aria-label="breadcrumb">
            <?php
        if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
        }
    ?>
        </nav> -->

        <!-- <div class="bg-title-breadcrumb">
            <h2><?php the_title(); ?></h2>
        </div> -->
    </div>
</div>

<main class="art">

    <div class="container">
        <div class="text-head-std">
            <?php the_content(); ?>
        </div>
    </div>

    <!-- start inne realizacje z tego działu -->

    <?php

$args = array(
    'post_type'      => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'order'          => 'ASC',
    // 'orderby'        => 'menu_order',
    'sort_column' => 'menu_order'
);


$parent = new WP_Query( $args );

if ( $parent->have_posts() ) : ?>

    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

    <section class="std-wykonczen">
        <div class="container-apla">
            <div class="std-bg-gray">
                <div class="container">
                    <a href="<?php the_permalink(); ?>">
                        <div class="row align-items-center">
                            <div class="col-md-6 col-one">
                                <div data-aos="fade-right" data-aos-duration="1500" class="col-info-img-standards">

                                    <?php $url = wp_get_attachment_url( get_post_thumbnail_id() );
							echo '<div style="background: url('. $url.'); background-repeat:no-repeat; background-position:center; background-size:cover;" class="img-bg-box1">'; ?>
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6 col-two">
                            <div class="std-desc">
                                <h2><?php the_title(); ?></h2>

                                <div class="text-small-std">
                                    <p><?php echo wp_trim_words( get_the_content(), 40, '...' );?></p>
                                </div>
                                <span class="btn-more-realizations">Zobacz więcej</span>

                            </div>
                        </div>
                </div>
                </a>
            </div>

        </div>
        </div>
    </section>

    <?php endwhile; ?>
    <?php endif; wp_reset_postdata(); ?>
    <!-- end inne realizacje -->


</main>
<?php get_footer(); ?>