<?php 
/* 
Template Name: Szablon podstron zwykłych
*/ 
?>

<?php get_header() ?>
<main id="realization-portfolio">
    <div style="background-image:url(<?php the_field('bg_top_subpage') ?>);" class="breadcrumb-bg">
        <div class="container-main">
            <!-- <nav class="breadcrumb d-flex align-items-center" aria-label="breadcrumb">
            <?php
        if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
        }
    ?>
        </nav> -->

        </div>
    </div>
    <section class="realizations-desc mb-40">
        <div class="container">
            <div class="text-realization">
                <?php the_content(); ?>
            </div>
        </div>
    </section>


    <!-- start inne realizacje z tego działu -->



    <section class="realizations">
        <div class="container-main">
            <div data-aos="zoom-out-up" data-aos-duration="1500" class="row aos-init aos-animate">


                <?php

$args = array(
    'post_type'      => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
 );


$parent = new WP_Query( $args );

if ( $parent->have_posts() ) : ?>

                <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

                <div class="col-md-6 col-lg-4 mb-95">
                    <div class="container-img">
                        <a href="<?php the_permalink(); ?>">
                            <!-- <?php the_post_thumbnail('', array('class' => 'img-fluid')); ?> -->
                            <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							echo '<div style="background: url('. $url.'); background-repeat:no-repeat; background-position:center; background-size:cover;" class="img-bg-box1">'; ?>
                    </div>
                    <div class="overlay">
                        <div class="text">
                            <h3><?php the_title(); ?></h3>
                            <p><?php echo wp_trim_words( get_the_content(), 15, '...' );?></p>
                        </div>
                    </div>
                    </a>
                </div>
                <span class="realization-title">
                    <h3><?php echo wp_trim_words( get_the_title(), 5, '...' );?></h3>
                </span>
            </div>
            <?php endwhile; ?>
            <?php endif; wp_reset_postdata(); ?>
        </div>



        </div>

        </div>
    </section>
</main>





<?php get_footer(); ?>