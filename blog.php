<?php
/** Template Name: Blog - lista wpisów
**/
?>
<?php get_header() ?>

<main id="offer">

    <section class="hero">
        <?php $heroHomeSlider = get_field('slider-home-blog'); ?>
        <img src="<?php echo $heroHomeSlider['sizes']['slider-home']; ?>"
            alt="<?php echo esc_attr($heroHomeSlider['alt']); ?>">

        <div class="bg-slide">
            <div class="container">
                <div class="text-wrapper-hero">
                    <?php if( get_field('title_slide-homepage-item-blog') ): ?>
                    <h1><?php the_field('title_slide-homepage-item-blog') ?></h1>
                    <?php endif; ?>
                    <?php if( get_field('text_slide-homepage-item-blog') ): ?>
                    <?php the_field('text_slide-homepage-item-blog') ?>
                    <?php endif; ?>
                    <div class="btn-wrapper">
                        <?php if( get_field('slide-homepage-item-btn-txt-blog') ): ?>
                        <a class="btn"
                            href="<?php the_field('slide-homepage-item-link-blog') ?>"><?php the_field('slide-homepage-item-btn-txt-blog') ?></a>
                        <?php endif; ?>
                        <?php if( get_field('slide-homepage-item-btn-txt2-blog') ): ?>
                        <a class="btn btn-transparent"
                            href="<?php the_field('slide-homepage-item-link2-blog') ?>"><?php the_field('slide-homepage-item-btn-txt2-blog') ?></a>
                        <?php endif; ?>
                    </div>
                </div>
                <a href="#sensowni-fotografia" class="scroll-down"><img
                        src="/app/themes/sensowni/assets/src/img/arrow-down.svg"></a>

                <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb( '<div id="breadcrumbs">','</div>' );
                    }
                ?>
            </div>
        </div>
    </section>



    <?php
if( get_field('wlacznik_sekcji_gornej_posty_blog') ) { ?>
    <section id="sensowni-fotografia" class="repeater-primary-home">
        <div class="container">
            <div class="head-wrapper bottom">
                <h2><?php the_field('tytul_sekcji_górnej_glowny_repeater_blog') ?></h2>
                <?php the_field('text_sekcji_gornej_glowny_repeater_blog') ?>
            </div>
        </div>
    </section>
    <?php } ?>

    <section id="sensowni-fotografia" class="repeater-primary-home">
        <div class="container">
            <div class="filter-area">
                <?php echo do_shortcode('[searchandfilter id="6517"]') ?>
            </div>
        </div>
        <?php echo do_shortcode('[searchandfilter id="6517" show="results"]') ?>
    </section>

    <?php
if( get_field('wlacznik_dodatkowej_sekcji_sekcji_gornej_posty_blog') ) { ?>
    <section id="sensowni-fotografia" class="repeater-primary-home">
        <div class="container">
            <div class="head-wrapper full-width">
                <?php the_field('dodatkowa_sekcja_tekstowa_blog_allPosts') ?>
            </div>
        </div>
    </section>
    <?php } ?>

</main>



<?php get_footer(); ?>