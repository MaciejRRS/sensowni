<?php 
/* 
Template Name: Strona Główna 
*/ 
?>

<?php get_header() ?>

<main id="home">
    <!-- start piimary repeater -->
    <?php
if( have_rows('glowny_repeater_homepage') ):
    while ( have_rows('glowny_repeater_homepage') ) : the_row();
?>
    <?php   $get_selection_section = get_sub_field('wybierz_sekcje_primary_repeater'); 
            if ($get_selection_section == 'hero'){ ?>

    <section class="hero">
        <?php $heroHomeSlider = get_sub_field('slide-homepage-item'); ?>
        <img src="<?php echo $heroHomeSlider['sizes']['slider-home']; ?>"
            alt="<?php echo esc_attr($heroHomeSlider['alt']); ?>">

        <div class="bg-slide">
            <div class="container">
                <div class="text-wrapper-hero" data-aos="fade-right">
                    <?php if( get_sub_field('title_slide-homepage-item') ): ?>
                    <h1><?php the_sub_field('title_slide-homepage-item') ?></h1>
                    <?php endif; ?>
                    <?php if( get_sub_field('text_slide-homepage-item') ): ?>
                    <?php the_sub_field('text_slide-homepage-item') ?>
                    <?php endif; ?>
                    <div class="btn-wrapper">
                        <?php if( get_sub_field('slide-homepage-item-btn-txt') ): ?>
                        <a class="btn"
                            href="<?php the_sub_field('slide-homepage-item-link') ?>"><?php the_sub_field('slide-homepage-item-btn-txt') ?></a>
                        <?php endif; ?>
                        <?php if( get_sub_field('slide-homepage-item-btn-txt2') ): ?>
                        <a class="btn btn-transparent"
                            href="<?php the_sub_field('slide-homepage-item-link2') ?>"><?php the_sub_field('slide-homepage-item-btn-txt2') ?></a>
                        <?php endif; ?>
                    </div>
                </div>
                <a href="#sensowni-fotografia" class="scroll-down"><img
                        src="/app/themes/sensowni/assets/src/img/arrow-down.svg"></a>
                <?php
                    if( get_sub_field('wlacz_kategorie_stron_hero') ) { ?>
                <ul class="list_category_link">
                    <?php
                    if( have_rows('klikalne_kategorie_do_stron') ):
                        while ( have_rows('klikalne_kategorie_do_stron') ) : the_row();
                ?>
                    <li><a
                            href="<?php the_sub_field('link_kategorii_hero') ?>"><?php the_sub_field('nazwa_kategorii_hero') ?></a>
                    </li>
                    <?php 
                    endwhile;
                    else :
                    endif;
                ?>
                </ul>
                <?php } ?>


            </div>
        </div>
    </section>

    <?php   } else if ($get_selection_section == 'section1') { ?>
    <section id="sensowni-fotografia"
        class="repeater-primary-home <?php if( get_sub_field('wlacz_szary_background') ) { ?>bg-grey <?php } ?>">
        <div class="container">
            <div class="head-wrapper" data-aos="fade-up" data-aos-duration="1000">
                <h2><?php the_sub_field('tytul_sekcji_górnej_glowny_repeater') ?></h2>
                <?php the_sub_field('text_sekcji_gornej_glowny_repeater') ?>
            </div>
            <div class="block-two-images" data-aos="fade-up" data-aos-duration="1000">
                <div class="column-left">
                    <?php $imgHomePortrait = get_sub_field('zdjecie_pionowe_glownej_sekcji_repeater'); ?>
                    <img src="<?php echo $imgHomePortrait['sizes']['img-thumb-portrait']; ?>"
                        alt="<?php echo esc_attr($imgHomePortrait['alt']); ?>">
                </div>
                <div class=" column-right">
                    <h4><?php the_sub_field('tytul_kategorii_obok_zdj_kwadratowe_glownej_sekcji_repeater') ?></h4>
                    <h3> <?php the_sub_field('tytul_obok_zdj_kwadratowe_glownej_sekcji_repeater') ?></h3>
                    <div class="column-right-wrapper">
                        <div class="column-left-img">
                            <?php $imgHomeWide = get_sub_field('zdjecie_kwadratowe_glownej_sekcji_repeater'); ?>
                            <img src="<?php echo $imgHomeWide['sizes']['img-thumb-wide']; ?>"
                                alt="<?php echo esc_attr($imgHomeWide['alt']); ?>">
                        </div>
                        <div class="column-right-text">
                            <?php the_sub_field('text_obok_zdj_kwadratowe_glownej_sekcji_repeater') ?>
                            <div class="btn-wrapper">

                                <?php if( get_sub_field('przycisk_1_tekst_obok_zdj_kwadratowe_glownej_sekcji_repeater') ): ?>
                                <a href="<?php the_sub_field('przycisk_1_link_obok_zdj_kwadratowe_glownej_sekcji_repeater') ?>"
                                    class="btn"><?php the_sub_field('przycisk_1_tekst_obok_zdj_kwadratowe_glownej_sekcji_repeater') ?></a>
                                <?php endif; ?>

                                <?php if( get_sub_field('przycisk_2_tekst_obok_zdj_kwadratowe_glownej_sekcji_repeater') ): ?>
                                <a href="<?php the_sub_field('przycisk_2_link_obok_zdj_kwadratowe_glownej_sekcji_repeater') ?>"
                                    class="btn btn-white"><?php the_sub_field('przycisk_2_tekst_obok_zdj_kwadratowe_glownej_sekcji_repeater') ?></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php  } else if ($get_selection_section == 'section2') { ?>
    <section id="sensowni-fotografia"
        class="repeater-primary-home <?php if( get_sub_field('wlacz_szary_background') ) { ?>bg-grey <?php } ?>">
        <div class="container">
            <div class="head-wrapper bottom" data-aos="fade-up" data-aos-duration="1000">
                <h2><?php the_sub_field('tytul_sekcji_dolnej_glowny_repeater') ?></h2>
                <?php the_sub_field('tekst_sekcji_dolnej_glowny_repeater') ?>
            </div>

            <div
                class="images-repeater-sesion <?php if( get_sub_field('enable_hover_img_overly') ) { ?>sesion-offer <?php } ?>">
                <?php
                if( have_rows('repeater_zdjecia_pionowe_glowny_repeater') ):
                    while ( have_rows('repeater_zdjecia_pionowe_glowny_repeater') ) : the_row();
                ?>
                <a href="<?php the_sub_field('link_do_sesji_wew_glowny_repeater'); ?>" class="img-content"
                    data-aos="fade-up" data-aos-duration="1000">
                    <?php $imgSesionPortrait = get_sub_field('zdjecie_pionowe_wew_glowny_repeater'); ?>
                    <img src="<?php echo $imgSesionPortrait['sizes']['img-thumb-portrait']; ?>"
                        alt="<?php echo esc_attr($imgSesionPortrait['alt']); ?>">
                    <div class="text">
                        <h4><?php the_sub_field('nazwa_kategorii_sesji_wew_glowny_repeater'); ?></h4>
                        <h3><?php the_sub_field('tytul_sesji_wew_glowny_repeater_kopia'); ?></h3>
                        <span class="btn">sprawdź ofertę >></span>
                    </div>
                    <div class="overly">
                        <span class="line-1"></span>
                        <span class="line-2"></span>
                        <span class="line-3"></span>
                        <span class="line-4"></span>
                    </div>
                </a>

                <?php 
                endwhile;
                else :
                endif;
            ?>
                <?php if( get_sub_field('przycisk_tekst_zdjecia_pionowe_glowny_repeater') ): ?>
                <div class="btn-wrapper btn-right">
                    <a href="<?php the_sub_field('przycisk_link_zdjecia_pionowe_glowny_repeater') ?>"
                        class="btn"><?php the_sub_field('przycisk_tekst_zdjecia_pionowe_glowny_repeater') ?></a>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </section>


    <!-- sekcja lewo-prawo -->
    <?php  } else if ($get_selection_section == 'section3') { ?>
    <section id="sensowni-fotografia"
        class="repeater-primary-home <?php if( get_sub_field('wlacz_szary_background') ) { ?>bg-grey <?php } ?>">
        <div class="container">
            <div class="head-wrapper bottom" data-aos="fade-up" data-aos-duration="1000">
                <h2><?php the_sub_field('tytul_sekcji_lewo_prawo_glowny_repeater') ?></h2>
                <?php the_sub_field('tekst_sekcji_lewo_prawo_glowny_repeater') ?>
            </div>

            <?php
                if( have_rows('lista_blokow_lewo_prawo_glowny_repeater') ):
                    while ( have_rows('lista_blokow_lewo_prawo_glowny_repeater') ) : the_row();
                ?>
            <div class="images-repeater-odd-even <?php if( get_sub_field('wlacz_zdjecie_z_ramka_zacznij_od_lewej') ) { ?> enable-border-img-left-right <?php } ?>"
                data-aos="fade-up" data-aos-duration="1000">
                <div class="column-left_odd_even">
                    <h4><?php the_sub_field('nazwa_kategorii_blok_lewo_prawo_glowny_repeater') ?></h4>
                    <h3><?php the_sub_field('tytul_blok_lewo_prawo_glowny_repeater') ?></h3>
                    <div class="excerpt">
                        <?php the_sub_field('zajawka_lewo_prawo_glowny_repeater') ?>
                    </div>
                    <div class="btn-wrapper">
                        <?php if( get_sub_field('przycisk_1_tekst_blok_lewo_prawo_glowny_repeater') ): ?>
                        <a href="<?php the_sub_field('przycisk_1_link_blok_lewo_prawo_glowny_repeater') ?>"
                            class="btn"><?php the_sub_field('przycisk_1_tekst_blok_lewo_prawo_glowny_repeater') ?></a>
                        <?php endif; ?>
                        <?php if( get_sub_field('przycisk_2_tekst_blok_lewo_prawo_glowny_repeater') ): ?>
                        <a href="<?php the_sub_field('przycisk_2_link_blok_lewo_prawo_glowny_repeater') ?>"
                            class="btn btn-white"><?php the_sub_field('przycisk_2_tekst_blok_lewo_prawo_glowny_repeater') ?></a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="column-right_odd_even">
                    <a href="<?php the_sub_field('przycisk_1_link_blok_lewo_prawo_glowny_repeater'); ?>"
                        class="img-content">
                        <?php $imgSesionWide = get_sub_field('zdjecie_bloku_lewo_prawo_glowny_repeater'); ?>

                        <?php if ( get_sub_field( 'wlacz_zdjecie_z_ramka_zacznij_od_lewej' ) ): ?>

                        <img src="<?php echo $imgSesionWide['sizes']['section-image-624x433']; ?>"
                            alt="<?php echo esc_attr($imgSesionWide['alt']); ?>">

                        <?php else: ?>

                        <img src="<?php echo $imgSesionWide['sizes']['img-thumb-wide']; ?>"
                            alt="<?php echo esc_attr($imgSesionWide['alt']); ?>">
                        <?php endif; ?>
                    </a>
                </div>
            </div>
            <?php 
                endwhile;
                else :
                endif;
            ?>


        </div>
    </section>



    <!-- section4 sekcja SEO -->
    <?php  } else if ($get_selection_section == 'secton4') { ?>
    <section id="sensowni-fotografia"
        class="repeater-primary-home <?php if( get_sub_field('wlacz_szary_background') ) { ?>bg-grey <?php } ?>"
        data-aos="fade-up" data-aos-duration="1000">
        <div class="container">
            <?php if( get_sub_field('wlacz_zdjecie_sekcja_seo') ) { ?>
            <a href="<?php the_sub_field('link_na_zdjeciu_sprawdz_oferte_sekcja_seo'); ?>" class="img-link">
                <div class="img-wrapper-seo">
                    <?php $imgSesionSEO = get_sub_field('zdjecie_sekcja_seo'); ?>
                    <img src="<?php echo $imgSesionSEO['sizes']['img-thumb-1060']; ?>"
                        alt="<?php echo esc_attr($imgSesionSEO['alt']); ?>">
                    <div class="text-content">
                        <h4><?php the_sub_field('tytul_na_zdjeciu_sekcja_seo') ?></h4>
                        <span
                            class="text-button"><?php the_sub_field('tytul_na_zdjeciu_sprawdz_oferte_sekcja_seo') ?></span>
                    </div>
                </div>
            </a>
            <?php } ?>
            <div class="head-wrapper bottom">
                <h2><?php the_sub_field('naglowek_sekcja_seo') ?></h2>
                <?php the_sub_field('tekst_sekcja_seo') ?>
            </div>
            <div class="btn-wrapper btn-wrapper-seo">
                <?php if( get_sub_field('przycisk_1_tekst_sekcja_seo') ): ?>
                <a href="<?php the_sub_field('przycisk_1_link_sekcja_seo') ?>"
                    class="btn"><?php the_sub_field('przycisk_1_tekst_sekcja_seo') ?></a>
                <?php endif; ?>
                <?php if( get_sub_field('przycisk_2_tekst_sekcja_seo') ): ?>
                <a href="<?php the_sub_field('przycisk_2_link_sekcja_seo') ?>"
                    class="btn btn-white"><?php the_sub_field('przycisk_2_tekst_sekcja_seo') ?></a>
                <?php endif; ?>
            </div>
        </div>
    </section>



    <!-- section4 sekcja SEO -->
    <?php  } else if ($get_selection_section == 'section5') { ?>
    <section id="sensowni-fotografia"
        class="repeater-primary-home <?php if( get_sub_field('wlacz_szary_background') ) { ?>bg-grey <?php } ?>">
        <div class="img-wrapper-big-picture-bg">


            <?php $imgSesionBigBictureBg = get_sub_field('zdjecie_100_procent_szerokosci_background'); ?>
            <img class="img-width-100" src="<?php echo $imgSesionBigBictureBg['sizes']['section-background-100']; ?>"
                alt="<?php echo esc_attr($imgSesionBigBictureBg['alt']); ?>">


            <div class="text-wrapper-bg">
                <div class="column-for-right" data-aos="fade-up" data-aos-duration="1000">
                    <h2><?php the_sub_field('tytul_na_tle_zdjecia_100_procent_szerokosci_background') ?></h2>
                    <?php the_sub_field('tekst_na_tle_zdjecia_100_procent_szerokosci_background') ?>
                    <?php if( get_sub_field('przycisk_tekst_na_tle_zdjecia_100_procent_szerokosci_background') ): ?>
                    <div class="btn-wrapper btn-right">
                        <a href="<?php the_sub_field('przycisk_link_na_tle_zdjecia_100_procent_szerokosci_background') ?>"
                            class="btn btn-transparent"><?php the_sub_field('przycisk_tekst_na_tle_zdjecia_100_procent_szerokosci_background') ?></a>
                    </div>
                    <?php endif; ?>
                </div>

            </div>






        </div>

    </section>


    <!-- blog 3 posty -->
    <?php  } else if ($get_selection_section == 'section6') { ?>
    <section id="sensowni-fotografia"
        class="repeater-primary-home <?php if( get_sub_field('wlacz_szary_background') ) { ?>bg-grey <?php } ?>">

        <div class="container">


            <div class="head-wrapper bottom" data-aos="fade-up" data-aos-duration="1000">
                <h2><?php the_sub_field('tytul_sekcji_z_blogiem') ?></h2>
                <?php the_sub_field('tekst_sekcji_z_blogiem') ?>
            </div>



            <div class="blog-posts">
                <?php $the_query = new WP_Query( array(
                'posts_per_page' => 3,
                'post_status' => 'publish',
                'post_type' => 'post'
            )); 
            ?>

                <?php if ( $the_query->have_posts() ) : ?>
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                <div class="column-post" data-aos="fade-up" data-aos-duration="1000">
                    <h4><?php the_category(', '); ?></h4>
                    <a href="<?php echo get_permalink(); ?>">
                        <div class="blog-title-area">
                            <h3 class="blog_post-title"><?php the_title(); ?></h3>
                        </div>
                        <div class="post-img">
                            <?php the_post_thumbnail( 'medium' ); ?>
                        </div>

                        <div class="blog_post-description">
                            <?php the_excerpt(); ?>
                        </div>
                        <span class="read-more"><?php the_field('blog_czytaj_wiecej_text_allPosts','options') ?>
                            >></span>
                    </a>


                </div>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>

                <?php else : ?>
                <p><?php __('No News'); ?></p>
                <?php endif; ?>

                <?php if( get_sub_field('button_all_blog_text') ): ?>
                <div class="btn-wrapper btn-right">
                    <a href="<?php the_sub_field('button_all_blog_link') ?>"
                        class="btn"><?php the_sub_field('button_all_blog_text') ?></a>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </section>










    <!-- start TABELA -->
    <?php  } else if ($get_selection_section == 'section7') { ?>
    <section id="sensowni-fotografia"
        class="repeater-primary-home <?php if( get_sub_field('wlacz_szary_background') ) { ?>bg-grey <?php } ?>">


        <div class="container">
            <?php if( get_sub_field('wlacz_naglowek_tekstowy_sekcji_tytul_i_tresc_tabela_pakietow') ) { ?>
            <div class="head-wrapper bottom">
                <h2><?php the_sub_field('tytul_sekcji_z_tabela_pakietow') ?></h2>
                <?php the_sub_field('tekst_sekcji_z_tabela_pakietow') ?>
            </div>
            <?php } ?>
            <div id="tabela" class="overflowTable" style="overflow-x:auto;">
                <table>
                    <?php
                if( have_rows('kolumna_tabeli_pakietow') ):
                    while( have_rows('kolumna_tabeli_pakietow') ) : the_row(); ?>
                    <tr>


                        <?php
                if( have_rows('wiersz_tabeli_pakietow') ):
                    while( have_rows('wiersz_tabeli_pakietow') ) : the_row(); ?>
                        <th>
                            <?php 
                            if( get_sub_field('tabela_wybor_pola') == 'pole_tekst' ) { ?>
                            <div><?php the_sub_field('komorka_kolumny_pakiet') ?>


                                <?php if( get_sub_field('komorka_kolumny_pakiet_link') ): ?>
                                <a
                                    href="<?php the_sub_field('komorka_kolumny_pakiet_link') ?>"><?php the_sub_field('komorka_kolumny_pakiet_link_name') ?></a>
                                <?php endif; ?>
                            </div>


                            <?php if( get_sub_field('tooltip_tekst') ): ?>
                            <img data-toggle="tooltip" data-placement="right"
                                title="<?php the_sub_field('tooltip_tekst') ?>"
                                src="<?php the_field('ikona_info_pakiety_tabela','options') ?>">
                            <?php endif; ?>
                            <?php } 

                            if( get_sub_field('tabela_wybor_pola') == 'pole_zawiera' ) {
                                if( get_sub_field('tabela_pole_tzawiera') ) {?>
                            <img src="<?php the_field('ikona__table_check','options') ?>">
                            <?php }
                                else { ?>
                            <img src="<?php the_field('ikona__table_minus','options') ?>">
                            <?php }
                            } ?>
                        </th>

                        <?php
                endwhile;
                else :
                endif;
                ?>
                    </tr>
                    <?php
                endwhile;
                else :
                endif;
            ?>
                </table>
            </div>
        </div>
    </section>
    <!-- END TABELA -->


    <!-- start field WYSWYG for gallery -->
    <?php  } else if ($get_selection_section == 'sections8') { ?>
    <section id="sensowni-fotografia"
        class="repeater-primary-home <?php if( get_sub_field('wlacz_szary_background') ) { ?>bg-grey <?php } ?>">
        <div class="container">
            <div class="head-wrapper full-width">
                <?php the_sub_field('dodatkowa_sekcja_na_galerie') ?>
            </div>
        </div>
    </section>



    <!-- start formularz kontaktowy -->
    <?php  } else if ($get_selection_section == 'section9') { ?>
    <section id="sensowni-fotografia"
        class="repeater-primary-home <?php if( get_sub_field('wlacz_szary_background') ) { ?>bg-grey <?php } ?>">
        <div class="container">
            <div class="head-wrapper">
                <?php echo do_shortcode(get_sub_field('shortcode_contact_form')); ?>
            </div>
        </div>
    </section>
    <?php  } ?>


    <!-- end primary repeater -->
    <?php 
                endwhile;
                else :
                endif;
            ?>
</main>



<?php get_footer(); ?>