<?php get_header() ?>

<main id="offer">

    <section class="hero">
        <?php $heroHomeSlider = get_field('slider-home-blog'); ?>
        <img src="<?php echo $heroHomeSlider['sizes']['slider-home']; ?>"
            alt="<?php echo esc_attr($heroHomeSlider['alt']); ?>">

        <div class="bg-slide">
            <div class="container">
                <div class="text-wrapper-hero">
                    <?php if( get_field('title_slide-homepage-item-blog') ): ?>
                    <h2><?php the_field('title_slide-homepage-item-blog') ?></h2>
                    <?php endif; ?>
                    <?php if( get_field('text_slide-homepage-item-blog') ): ?>
                    <?php the_field('text_slide-homepage-item-blog') ?>
                    <?php endif; ?>
                    <div class="btn-wrapper">
                        <?php if( get_field('slide-homepage-item-btn-txt-blog') ): ?>
                        <a class="btn"
                            href="<?php the_field('slide-homepage-item-link-blog') ?>"><?php the_field('slide-homepage-item-btn-txt-blog') ?></a>
                        <?php endif; ?>
                        <?php if( get_field('slide-homepage-item-btn-txt2-blog') ): ?>
                        <a class="btn btn-transparent"
                            href="<?php the_field('slide-homepage-item-link2-blog') ?>"><?php the_field('slide-homepage-item-btn-txt2-blog') ?></a>
                        <?php endif; ?>
                    </div>
                </div>
                <a href="#sensowni-fotografia" class="scroll-down"><img
                        src="/app/themes/sensowni/assets/src/img/arrow-down.svg"></a>

                <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb( '<div id="breadcrumbs">','</div>' );
                    }
                ?>
            </div>
        </div>
    </section>


    <section id="sensowni-fotografia" class="repeater-primary-home">
        <div class="container">
            <div class="head-wrapper full-width">
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>
            </div>
        </div>
    </section>







    <section id="sensowni-fotografia" class="repeater-primary-home">
        <div class="container">
            <?php
            if( get_field('wlacz_naglowek_wiecej_na_blogu') ) { ?>
            <div class="head-wrapper full-width">
                <h2><?php the_field('naglowek_wiecej_na_blogu') ?></h2>
            </div>
            <?php } ?>

            <div class="blog-posts">
                <?php $category = get_the_category(); ?>
                <?php $the_query = new WP_Query( array(
                'posts_per_page' => 6,
                'post_status' => 'publish',
                'post_type' => 'post',
                'category_name' => $category[0]->cat_name,
            )); 
            ?>

                <?php if ( $the_query->have_posts() ) : ?>
                <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                <div class="column-post">
                    <h4><?php the_category(', '); ?></h4>
                    <a href="<?php echo get_permalink(); ?>">
                        <div class="blog-title-area">
                            <h3 class="blog_post-title"><?php the_title(); ?></h3>
                        </div>
                        <div class="post-img">
                            <?php the_post_thumbnail( 'medium' ); ?>
                        </div>

                        <div class="blog_post-description">
                            <?php the_excerpt(); ?>
                        </div>
                        <span class="read-more"><?php the_field('blog_czytaj_wiecej_text_allPosts','options') ?>
                            >></span>
                    </a>
                </div>

                <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>

                <?php else : ?>
                <p><?php __('No News'); ?></p>
                <?php endif; ?>

                <?php if( get_sub_field('button_all_blog_text') ): ?>
                <div class="btn-wrapper btn-right">
                    <a href="<?php the_sub_field('button_all_blog_link') ?>"
                        class="btn"><?php the_sub_field('button_all_blog_text') ?></a>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </section>
</main>


<?php get_footer(); ?>