<?php

// Register Custom Navigation Walker
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';


register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'sensowni' ),
	'secondary' => __( 'Secondary Menu', 'sensowni' ),
) );


/*
	 * Enable support for custom logo.
	 *
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 400,
		'width'       => 100,
		'flex-height' => true,
		'flex-width'  => true,
	) );

// Register thumbnails
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'homepage-thumb', 385, 302 ); // Soft Crop Mode
	add_image_size( 'slider-home', 1920, 780 );
	add_image_size( 'img-thumb-portrait', 405, 600 );
	add_image_size( 'img-thumb-wide', 515, 360 );
	add_image_size( 'img-thumb-1060', 1060, 589 );
	add_image_size( 'logo-scroll', 188, 27 );
	add_image_size( 'section-background-100', 1920, 927 );
	add_image_size( 'section-image-624x433', 624, 433 );
	add_image_size( 'section-image-600x653', 600, 653 );




add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
  if (in_array('current-menu-item', $classes) ){
    $classes[] = 'active ';
  }
  return $classes;
}


function add_stylesheets_and_scripts()
{
wp_enqueue_style( 'style', get_stylesheet_uri(), array(), filemtime(get_template_directory() . '/style.css'), 'all' );
wp_enqueue_style('custom', get_template_directory_uri() . '/assets/dist/css/main.min.css', array(), filemtime(get_template_directory() . '/assets/dist/css/main.min.css'), 'all' );

wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/src/js/libr/jquery.min.js', array(), null, true);
// wp_enqueue_script('popper', get_template_directory_uri() . '/assets/src/js/libr/popper.min.js', array(), null, true);
wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/src/js/libr/bootstrap.bundle.min.js', array(), null, true);
wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/src/js/main.js', array(), null, true );

global $template;

// only for o-firmie.php hompage.php, 

// if ((basename($template) === 'o-firmie.php') || (basename($template) === 'homesite.php') ){
//     wp_enqueue_style( 'swiper-css', get_template_directory_uri() . '/assets/src/css/swiper/swiper-bundle.min.css' );
//     wp_enqueue_script( 'swiper-js', get_template_directory_uri() . '/assets/src/js/swiper/swiper-bundle.min.js', array(), null, true );
//     wp_enqueue_script( 'sctipts-about', get_template_directory_uri() . '/assets/src/js/swiper/scripts-about.js', array(), null, true );
// }

// add animate AOS for all pages
    wp_enqueue_style( 'aos-css', get_template_directory_uri() . '/assets/src/css/aos.css' );
    wp_enqueue_script( 'aos-js', get_template_directory_uri() . '/assets/src/js/aos.js', array(), null, true );


}
add_action('wp_enqueue_scripts', 'add_stylesheets_and_scripts');


    
//add option page to panel (ACF)
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page('Stopka');
	acf_add_options_page('Logo scroll');
	acf_add_options_page('Przyklejony pasek w stopce');
	acf_add_options_page('Ikony tabeli pakietów');
	acf_add_options_page('Przycisk (czytaj więcej) na blogu');
}


// delete p from contact form 7
add_filter('wpcf7_autop_or_not', '__return_false');


//** *Enable upload for webp image files.*/
function webp_upload_mimes($existing_mimes) {
    $existing_mimes['webp'] = 'image/webp';
    return $existing_mimes;
}
add_filter('mime_types', 'webp_upload_mimes');

//** * Enable preview / thumbnail for webp image files.*/
function webp_is_displayable($result, $path) {
    if ($result === false) {
        $displayable_image_types = array( IMAGETYPE_WEBP );
        $info = @getimagesize( $path );

        if (empty($info)) {
            $result = false;
        } elseif (!in_array($info[2], $displayable_image_types)) {
            $result = false;
        } else {
            $result = true;
        }
    }

    return $result;
}
add_filter('file_is_displayable_image', 'webp_is_displayable', 10, 2);

function filter_acf_the_content( $value ) {
if ( class_exists( 'iworks_orphan' )) {
$orphan = new iworks_orphan();
$value = $orphan->replace( $value );
}
 
return $value;
};






// Register our widget areas.
register_sidebar( array(
	'name'          => __( 'Stopka: blok 1', 'sensowni_new' ),
	'id'            =>'sanfooter-1',
	'description'   => __( 'Tutaj należy dodać widgety.', 'redrocks' ),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget'  => '</aside>',
	'before_title'  => '<span class="widget-title">',
	'after_title'   => '</span>',
) );

register_sidebar( array(
	'name'          => __( 'Stopka: blok 2', 'sensowni_new' ),
	'id'            =>'sanfooter-2',
	'description'   => __( 'Tutaj należy dodać widgety.', 'redrocks' ),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget'  => '</aside>',
	'before_title'  => '<span class="widget-title">',
	'after_title'   => '</span>',
) ); 

register_sidebar( array(
	'name'          => __( 'Stopka: blok 3', 'sensowni_new' ),
	'id'            =>'sanfooter-3',
	'description'   => __( 'Tutaj należy dodać widgety.', 'redrocks' ),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget'  => '</aside>',
	'before_title'  => '<span class="widget-title">',
	'after_title'   => '</span>',
) ); 

register_sidebar( array(
	'name'          => __( 'Stopka: blok 4', 'sensowni_new' ),
	'id'            =>'sanfooter-4',
	'description'   => __( 'Tutaj należy dodać widgety.', 'redrocks' ),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget'  => '</aside>',
	'before_title'  => '<span class="widget-title">',
	'after_title'   => '</span>',
) ); 



// deregister css and js contact from 7 (scripts working only for page id 22, 65)
add_action( 'wp_print_scripts', 'deregister_cf7_javascript', 100 );
function deregister_cf7_javascript() {
    if ( !is_page(array(6564,169, 6643, 6645, 6647, 6649, 6651, 6653, 6655, 6657, 6659, 6661, 6663, 6665)) ) {
        wp_deregister_script( 'contact-form-7' );
    }
}
add_action( 'wp_print_styles', 'deregister_cf7_styles', 100 );
function deregister_cf7_styles() {
    if ( !is_page(array(6564,169, 6643, 6645, 6647, 6649, 6651, 6653, 6655, 6657, 6659, 6661, 6663, 6665)) ) {
        wp_deregister_style( 'contact-form-7' );
    }
}



// deregister css and js reCaptcha v3 (scripts working only for page id 22, 65)
function oiw_disable_recaptcha_badge_post(){
	if ( !is_page(array(6564,169, 6643, 6645, 6647, 6649, 6651, 6653, 6655, 6657, 6659, 6661, 6663, 6665) ) ) {
	   wp_dequeue_script('google-recaptcha');
	   add_filter( 'wpcf7_load_js', '__return_false' );
	   add_filter( 'wpcf7_load_css', '__return_false' );
	   remove_action( 'wp_enqueue_scripts', 'wpcf7_recaptcha_enqueue_scripts', 20 );
	}
 }
 add_action( 'wp_enqueue_scripts', 'oiw_disable_recaptcha_badge_post' );




// deregister search & filter css i js
function remove_sf_scripts() {
	if ( !is_page(array(2037) ) ) {
		wp_deregister_script( 'search-filter-plugin-build' );
		wp_deregister_script( 'search-filter-plugin-chosen' );
		wp_deregister_script( 'jquery-ui-datepicker' );
		wp_deregister_style( 'search-filter-plugin-styles' );
	}
}
add_action('wp_enqueue_scripts', 'remove_sf_scripts', 100);




add_filter('bcn_breadcrumb_url', 'my_breadcrumb_url_changer', 3, 10);
function my_breadcrumb_url_changer($url, $type, $id)
{
    if(in_array('sections', $type))
    {
        $url = str_replace("sections/", "", $url);
    }
    return $url;
}



 // debug for worpdress
ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );
define('WP_DEBUG', false);
define('WP_DEBUG_DISPLAY', false);




 