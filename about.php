<?php
/** Template Name: O nas
**/
?>
<?php get_header() ?>

<main id="offer">
    <section class="hero">
        <?php $heroHomeSlider = get_field('slider-home-about'); ?>
        <img src="<?php echo $heroHomeSlider['sizes']['slider-home']; ?>"
            alt="<?php echo esc_attr($heroHomeSlider['alt']); ?>">

        <div class="bg-slide">
            <div class="container">
                <div class="text-wrapper-hero">
                    <?php if( get_field('title_slide-homepage-item-about') ): ?>
                    <h1><?php the_field('title_slide-homepage-item-about') ?></h1>
                    <?php endif; ?>
                    <?php if( get_field('text_slide-homepage-item-about') ): ?>
                    <?php the_field('text_slide-homepage-item-about') ?>
                    <?php endif; ?>
                    <div class="btn-wrapper">
                        <?php if( get_field('slide-homepage-item-btn-txt-about') ): ?>
                        <a class="btn"
                            href="<?php the_field('slide-homepage-item-link-about') ?>"><?php the_field('slide-homepage-item-btn-txt-about') ?></a>
                        <?php endif; ?>
                        <?php if( get_field('slide-homepage-item-btn-txt2-about') ): ?>
                        <a class="btn btn-transparent"
                            href="<?php the_field('slide-homepage-item-link2-about') ?>"><?php the_field('slide-homepage-item-btn-txt2-about') ?></a>
                        <?php endif; ?>
                    </div>
                </div>
                <a href="#sensowni-fotografia" class="scroll-down"><img
                        src="/app/themes/sensowni/assets/src/img/arrow-down.svg"></a>

                <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb( '<div id="breadcrumbs">','</div>' );
                    }
                ?>
            </div>
        </div>
    </section>

    <section id="sensowni-fotografia" class="repeater-primary-home">
        <div class="container">
            <div class="head-wrapper bottom">
                <h2><?php the_field('naglowek_sekcji_pod_hero_o_nas') ?></h2>
                <?php the_field('tekst_sekcji_pod_hero_o_nas') ?>
            </div>
        </div>
    </section>


    <section id="sensowni-fotografia" class="repeater-primary-home">
        <div class="container">
            <?php if( have_rows('repeater_sekcji_o_nas') ):
            while( have_rows('repeater_sekcji_o_nas') ) : the_row(); ?>
            <div class="block-repeater-wrapper-about">
                <div class="columns-wrapper-top">
                    <div class="col-left-img">
                        <?php $imgPeopleAbout = get_sub_field('zdjecie_sekcji_o_nas_repeater'); ?>
                        <img src="<?php echo $imgPeopleAbout['sizes']['section-image-600x653']; ?>"
                            alt="<?php echo esc_attr($imgPeopleAbout['alt']); ?>">
                    </div>
                    <div class="col-right-text">
                        <h4 class="title-role"><?php the_sub_field('stanowisko_blok_sekcji_o_nas_repeater') ?></h4>
                        <div class="wrap-titleAnd-sm">
                            <h3 class="name"><?php the_sub_field('imie_blok_sekcji_o_nas_repeater') ?></h3>
                            <div class="sm-icon-wrapper">
                                <?php
                    if( have_rows('lista_ikon_social_media_about') ):
                        while( have_rows('lista_ikon_social_media_about') ) : the_row(); ?>
                                <a href="<?php the_sub_field('link_ikona_social_media_about') ?>" class="icon-sm"><img
                                        src="<?php the_sub_field('ikona_social_media_about') ?>"></a>
                                <?php
                                endwhile;
                                else :
                                endif;
                            ?>
                            </div>
                        </div>
                        <div class="text-field-wyswyg">
                            <?php the_sub_field('pole_na_tekst_about') ?>
                        </div>
                    </div>
                </div>
                <div class="column-wrapper-bottom">
                    <?php the_sub_field('pole_galeria_wyswyg_for_about') ?>
                </div>
            </div>
            <?php endwhile;
          else :
          endif;
    ?>
        </div>
    </section>


    <!-- start field WYSWYG for about -->
    <?php if( get_field('wlacz_dodatkowa_sekcje_naglowek_about_enable') ) { ?>
    <section id="sensowni-fotografia" class="repeater-primary-home">
        <div class="container">
            <div class="head-wrapper full-width">
                <?php the_field('dodatkowa_sekcja_naglowek_i_tekst_about') ?>
            </div>
        </div>
    </section>
    <?php } ?>

</main>



<?php get_footer(); ?>