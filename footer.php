<footer style="background-image: url(<?php the_field('tlo_stopki_footer_bg','options') ?>)">
    <div class="container">
        <div class="footer-content">
            <div class="cta_footer">
                <h2> <?php the_field('cta_tytul_footer','options') ?></h2>
                <?php the_field('cta_tekst_footer','options') ?>
                <div class="btn-wrapper">
                    <a class="btn" href="<?php the_field('cta_przycisk_link_footer','options') ?>">
                        <?php the_field('cta_przycisk_tekst_footer','options') ?></a>
                </div>
            </div>

            <div class="footer-columns">
                <?php
                    if( have_rows('lista_kolumn_stopki_footer','options') ):
                        while ( have_rows('lista_kolumn_stopki_footer','options') ) : the_row();
                ?>
                <div class="column-footer-item">
                    <?php if( get_sub_field('naglowek_stopki_column_footer','options') ): ?>
                    <h4><?php the_sub_field('naglowek_stopki_column_footer','options') ?></h4>
                    <?php endif; ?>


                    <?php      
                $get_selection_footer_field = get_sub_field('wybierz_typ_pola_footer','options'); 

                 if ($get_selection_footer_field == 'zakładka'){ ?>
                    <ul>
                        <?php
                    if( have_rows('lista_zakladek_columns_footer','options') ):
                        while ( have_rows('lista_zakladek_columns_footer','options') ) : the_row();
                ?>
                        <li><a href="<?php the_sub_field('link_zakladki_column_footer','options') ?>">
                                <?php the_sub_field('nazwa_zakladki_column_footer','options'); ?>
                            </a>
                        </li>

                        <?php 
                    endwhile;
                    else :
                    endif;
                ?>
                    </ul>
                    <?php  } else if ($get_selection_footer_field == 'wysywig') { ?>
                    <?php the_sub_field('pole_tekstowe_footer_wysywig','options'); ?>
                    <?php  } ?>


                </div>
                <?php 
                    endwhile;
                    else :
                    endif;
                ?>
            </div>
            <div class="bottom-footer">
                <div class="column-left-bottom-footer">
                    made with <span>❤</span> by <a href="https://redrocks.pl" target="_blank">RedRockS - strony
                        internetowe
                        Kraków</a>
                </div>
                <div class="column-right-bottom-footer">
                    <a href="https://sensowni.pl" target="_blank">Sensowni.pl - fotografia ©
                        <?php echo date("Y"); ?></a>
                </div>
            </div>
        </div>

    </div>
    <div class="foter-sticky">
        <div class="container">
            <div class="list-buttons">
                <?php
                    if( have_rows('list_column_sticky_footer','options') ):
                        while ( have_rows('list_column_sticky_footer','options') ) : the_row();
                ?>
                <a href="<?php the_sub_field('link_column_sticky_footer') ?>" class="column-footer-sticky">
                    <h5><?php the_sub_field('name_column_sticky_footer','options'); ?></h5>
                    <img src="<?php the_sub_field('icon_column_sticky_footer','options'); ?>">
                </a>
                <?php 
                    endwhile;
                    else :
                    endif;
                ?>

            </div>
        </div>
    </div>

</footer>


<?php wp_footer(); ?>



<script>
AOS.init();
</script>



<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-43853737-26"></script>
<script>
window.dataLayer = window.dataLayer || [];

function gtag() {
    dataLayer.push(arguments);
}
gtag('js', new Date());

gtag('config', 'UA-43853737-26');
</script> -->

</body>

</html>