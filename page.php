<?php get_header(); ?>


<section class="realizations-desc">

    <main id="main" class="site-main" role="main">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php
						// Start the loop.
						while ( have_posts() ) : the_post();
				
							// Include the page content template.
							//get_template_part( 'template-parts/content', 'page' );
						?>
                    <h2><?php the_title(); ?></h2>
                    <div class="text-realization mt-80">
                        <?php the_content(); ?>
                    </div>
                    <?php //If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) {
								comments_template();
						}
				
						endwhile;
					?>
                </div>
            </div>
    </main><!-- .site-main -->

</section>








</div><!-- .content-area -->



<?php get_footer(); ?>