<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<?php get_header('pages'); ?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<section class="recentPosts padTitle">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				<?php if ( have_posts() ) : ?>

				<?php if ( is_home() && ! is_front_page() ) : ?>
					<header>
						<h2 class="page-title screen-reader-text"><?php single_post_title(); ?></h2>
					</header>
				<?php endif; ?>
				<div class="row">
					<?php
					// Start the loop.
					while ( have_posts() ) :
						the_post();	?>
					
						<div class="col-md-4">
							<div class="card">
							<?php the_post_thumbnail('homesite-thumbnail', array('class' => 'card-img-top')); ?>
								<div class="card-body">
									<h5 class="card-title"><?php echo wp_trim_words( get_the_title(), 10, '...' ); ?></h5>
									<p class="card-text"><?php echo wp_trim_words( get_the_content(), 16, '...' ); ?></p>
									<a href="<?php echo get_permalink(); ?>" class="btn btn-more btn-absolute float-right">Czytaj dalej</a>
								</div>
							</div>
						</div>
						<?php	// End the loop.
						endwhile; ?>
				</div>

				<?php pagination(); /*wyświetlamy paginację*/ ?>
	<?php
	// If no content, include the "No posts found" template.
	else : ?>
	<h4>Brak wpisów na blogu</h4>

	<?php endif;
	?>
				</div>
			</div>
		</div>
		</section>

		</main><!-- .site-main -->
	</div><!-- .content-area -->


<?php get_footer(); ?>
