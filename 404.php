<?php
/**
 * The template for displaying 404 pages (Not Found)
 */ ?>

<?php
get_header();

?>
<div class="container not-found__container">
    <section class="text-center">
        <h1 class="not-found__title">404</h1>
        <h2 class="not-found__subtitle"> Strona której szukasz nie istnieje, sprawdź poprawność adresu url</h2>
        <a class="btn" href="/">Wróć do strony głównej</a>
    </section>
</div>
<?php

get_footer();
?>
<style>
header .header-container {
    background-color: black;
}


.not-found__container {
    margin: 150px auto 100px;
}

.text-center {
    text-align: center !important;
}

.not-found__title {
    color: #cc273f;
    font-weight: 400;
    font-size: 5rem !important;
}

.not-found__subtitle {
    font-weight: 400;
    font-size: 14px !important;
}
</style>