<?php
/**
* A Simple Category Template
*/
 
get_header(); ?> 
 
<section id="primary" class="recentPosts">
<div id="content" class="container" role="main">
 
<?php 
// Check if there are any posts to display
if ( have_posts() ) : ?>

<!-- display categories -->
<div class="categories-buttons">
						<div class="row">
							<?php
								$categories = get_categories();
								foreach ($categories as $cat) {
								$category_link = get_category_link($cat->cat_ID);
								echo '<div class="col-md-4"><a class="cat-link" href="'.esc_url( $category_link ).'" title="'.esc_attr($cat->name).'">'.$cat->name.'</a></div>';
								}
							?>
						</div>
					</div>
				<!-- display cat end --> 

<header class="archive-header">
<h2 class="archive-title">Kategoria: <?php single_cat_title( '', true ); ?></h2>

        
 
<?php
// Display optional category description
 if ( category_description() ) : ?>
<div class="archive-meta"><?php echo category_description(); ?></div>
<?php endif; ?>
</header>
<div class="row">
<?php
 
// The Loop
while ( have_posts() ) : the_post(); ?>
<!-- <div class="card">
  <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
</div>

 
<div class="entry">
  <p class="card-text"><?php echo wp_trim_words( get_the_content(), 15, '...' ); ?></p>
</div> -->


  <div class="col-md-4">
            <div class="card">
                    <a href="<?php echo get_permalink(); ?>">
                    <?php the_post_thumbnail('homesite-thumbnail', array('class' => 'img-fluid card-img-top')); ?>
                    <div class="card-body">
                        <h5 class="card-title"><?php echo wp_trim_words( get_the_title(), 10, '...' ); ?></h5>
                        <p class="card-text"><?php echo wp_trim_words( get_the_content(), 16, '...' ); ?></p>
                    </a>
                        <a href="<?php echo get_permalink(); ?>" class="btn btn-more btn-absolute float-right">Czytaj dalej</a>
                    </div>
            </div>
        </div>

 
<?php endwhile; ?> 

</div>
 
<?php else: ?>
<p>Przepraszamy, brak wpisów z tej katagorii</p>
 
 
<?php endif; ?>
</div>
</section>
 

<?php get_footer(); ?>
