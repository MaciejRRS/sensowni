<?php
/** Template Name: Oferta - kategorie - podstrona
**/
?>

<?php get_header() ?>

<main id="offer">

    <section class="hero">
        <?php $heroSubpageSlider = get_field('slider-home-subPage'); ?>
        <img src="<?php echo $heroSubpageSlider['sizes']['slider-home']; ?>"
            alt="<?php echo esc_attr($heroSubpageSlider['alt']); ?>">

        <div class="bg-slide">
            <div class="container">
                <div class="text-wrapper-hero">
                    <?php if( get_field('title_slide-homepage-item-subPage') ): ?>
                    <h1><?php the_field('title_slide-homepage-item-subPage') ?></h1>
                    <?php endif; ?>
                    <?php if( get_field('text_slide-homepage-item-subPage') ): ?>
                    <?php the_field('text_slide-homepage-item-subPage') ?>
                    <?php endif; ?>
                    <div class="btn-wrapper">
                        <?php if( get_field('slide-homepage-item-btn-txt-subPage') ): ?>
                        <a class="btn"
                            href="<?php the_field('slide-homepage-item-link-subPage') ?>"><?php the_field('slide-homepage-item-btn-txt-subPage') ?></a>
                        <?php endif; ?>
                        <?php if( get_field('slide-homepage-item-btn-txt2-subPage') ): ?>
                        <a class="btn btn-transparent"
                            href="<?php the_field('slide-homepage-item-link2-subPage') ?>"><?php the_field('slide-homepage-item-btn-txt2-subPage') ?></a>
                        <?php endif; ?>
                    </div>
                </div>
                <a href="#sensowni-fotografia" class="scroll-down"><img
                        src="/app/themes/sensowni/assets/src/img/arrow-down.svg"></a>

                <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                    yoast_breadcrumb( '<div id="breadcrumbs">','</div>' );
                    }
                ?>
            </div>
        </div>
    </section>



    <!-- section4 sekcja SEO -->
    <section class="repeater-primary-home" data-aos="fade-up" data-aos-duration="1000">
        <div class="container">
            <div class="head-wrapper bottom">
                <h2><?php the_field('naglowek_sekcja_seo_subpage') ?></h2>
                <?php the_field('tekst_sekcja_seo_subpage') ?>
            </div>
            <div class="btn-wrapper btn-wrapper-seo">
                <?php if( get_field('przycisk_1_tekst_sekcja_seo_subpage') ): ?>
                <a href="<?php the_field('przycisk_1_link_sekcja_seo_subpage') ?>"
                    class="btn"><?php the_field('przycisk_1_tekst_sekcja_seo_subpage') ?></a>
                <?php endif; ?>
                <?php if( get_field('przycisk_2_tekst_sekcja_seo_subpage') ): ?>
                <a href="<?php the_field('przycisk_2_link_sekcja_seo_subpage') ?>"
                    class="btn btn-white"><?php the_field('przycisk_2_tekst_sekcja_seo_subpage') ?></a>
                <?php endif; ?>
            </div>
        </div>
    </section>

    <?php
                if( have_rows('repeater_zdjecia_lewoprawo_i_tekst') ):
                    while ( have_rows('repeater_zdjecia_lewoprawo_i_tekst') ) : the_row();
                ?>
    <!-- sekcja lewo-prawo -->
    <section class="repeater-primary-home" data-aos="fade-up" data-aos-duration="1000">
        <div class="container">
            <div class="head-wrapper bottom">
                <h2><?php the_sub_field('tytul_sekcji_lewo_prawo_subPage') ?></h2>
                <?php the_sub_field('tekst_sekcji_lewo_prawo_subPage') ?>
            </div>

            <?php
                if( have_rows('lista_blokow_lewo_prawo_subPage1') ):
                    while ( have_rows('lista_blokow_lewo_prawo_subPage1') ) : the_row();
                ?>
            <div class="images-repeater-odd-even enable-border-img-left-right">
                <div class="column-left_odd_even">
                    <h4><?php the_sub_field('nazwa_kategorii_blok_lewo_prawo_glowny_repeater_subPage1') ?></h4>
                    <h3><?php the_sub_field('tytul_blok_lewo_prawo_glowny_repeater_subPage1') ?></h3>
                    <div class="excerpt">
                        <?php the_sub_field('zajawka_lewo_prawo_glowny_repeater_subPage1') ?>
                    </div>
                    <div class="btn-wrapper">
                        <?php if( get_sub_field('przycisk_1_tekst_blok_lewo_prawo_glowny_repeater_subPage1') ): ?>
                        <a href="<?php the_sub_field('przycisk_1_link_blok_lewo_prawo_glowny_repeater_subPage1') ?>"
                            class="btn"><?php the_sub_field('przycisk_1_tekst_blok_lewo_prawo_glowny_repeater_subPage1') ?></a>
                        <?php endif; ?>
                        <?php if( get_sub_field('przycisk_2_tekst_blok_lewo_prawo_glowny_repeater_subPage1') ): ?>
                        <a href="<?php the_sub_field('przycisk_2_link_blok_lewo_prawo_glowny_repeater_subPage1') ?>"
                            class="btn btn-white"><?php the_sub_field('przycisk_2_tekst_blok_lewo_prawo_glowny_repeater_subPage1') ?></a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="column-right_odd_even">
                    <a href="<?php the_sub_field('przycisk_1_link_blok_lewo_prawo_glowny_repeater_subPage1'); ?>"
                        class="img-content">
                        <?php $imgSesionWideSpb = get_sub_field('zdjecie_bloku_lewo_prawo_glowny_repeater_subPage1'); ?>


                        <img src="<?php echo $imgSesionWideSpb['sizes']['section-image-624x433']; ?>"
                            alt="<?php echo esc_attr($imgSesionWideSpb['alt']); ?>">
                    </a>
                </div>
            </div>
            <?php 
                endwhile;
                else :
                endif;
            ?>


        </div>
    </section>




    <!-- start field WYSWYG for gallery -->
    <section class="repeater-primary-home" data-aos="fade-up" data-aos-duration="1000">
        <div class="container">
            <div class="head-wrapper full-width">
                <?php the_sub_field('dodatkowa_sekcja_na_galerie_lub_txt_sbp') ?>
            </div>
        </div>
    </section>



    <?php 
                endwhile;
                else :
                endif;
            ?>





    <!-- start TABELA -->

    <section class="repeater-primary-home" data-aos="fade-up" data-aos-duration="1000">
        <div class="container">
            <?php if( get_field('wlacz_naglowek_tekstowy_sekcji_tytul_i_tresc_tabela_pakietow_spb') ) { ?>
            <div class="head-wrapper bottom">
                <h2><?php the_field('tytul_sekcji_z_tabela_pakietow_spb') ?></h2>
                <?php the_field('tekst_sekcji_z_tabela_pakietow_spb') ?>
            </div>
            <?php } ?>
            <div id="tabela" class="overflowTable" style="overflow-x:auto;">
                <table>
                    <?php
                if( have_rows('kolumna_tabeli_pakietow_kopia') ):
                    while( have_rows('kolumna_tabeli_pakietow_kopia') ) : the_row(); ?>
                    <tr>


                        <?php
                if( have_rows('wiersz_tabeli_pakietow_spb') ):
                    while( have_rows('wiersz_tabeli_pakietow_spb') ) : the_row(); ?>
                        <th>
                            <?php 
                            if( get_sub_field('tabela_wybor_pola_spb') == 'pole_tekst' ) { ?>
                            <div><?php the_sub_field('komorka_kolumny_pakiet_spb') ?>


                                <?php if( get_sub_field('komorka_kolumny_pakiet_link_spb') ): ?>
                                <a
                                    href="<?php the_sub_field('komorka_kolumny_pakiet_link_spb') ?>"><?php the_sub_field('komorka_kolumny_pakiet_link_name_spb') ?></a>
                                <?php endif; ?>
                            </div>


                            <?php if( get_sub_field('tooltip_tekst_spb') ): ?>
                            <img data-toggle="tooltip" data-placement="right"
                                title="<?php the_sub_field('tooltip_tekst') ?>"
                                src="<?php the_field('ikona_info_pakiety_tabela','options') ?>">
                            <?php endif; ?>
                            <?php } 

                            if( get_sub_field('tabela_wybor_pola_spb') == 'pole_zawiera' ) {
                                if( get_sub_field('tabela_pole_tzawiera_spb') ) {?>
                            <img src="<?php the_field('ikona__table_check','options') ?>">
                            <?php }
                                else { ?>
                            <img src="<?php the_field('ikona__table_minus','options') ?>">
                            <?php }
                            } ?>
                        </th>

                        <?php
                endwhile;
                else :
                endif;
                ?>
                    </tr>
                    <?php
                endwhile;
                else :
                endif;
            ?>
                </table>
            </div>
        </div>
    </section>
    <!-- END TABELA -->


    <!-- sekcja ze zdjęciami pionowymi -->
    <section id="sensowni-fotografia" class="repeater-primary-home">
        <div class="container">
            <div class="head-wrapper bottom" data-aos="fade-up" data-aos-duration="1000">
                <h2><?php the_field('tytul_sekcji_dolnej_sbp') ?></h2>
                <?php the_field('tekst_sekcji_dolnej_sbp') ?>
            </div>

            <div
                class="images-repeater-sesion <?php if( get_sub_field('enable_hover_img_overly_sbp') ) { ?>sesion-offer <?php } ?>">
                <?php
                if( have_rows('repeater_zdjecia_pionowe_glowny_repeater_sbp') ):
                    while ( have_rows('repeater_zdjecia_pionowe_glowny_repeater_sbp') ) : the_row();
                ?>
                <a href="<?php the_sub_field('link_do_sesji_wew_glowny_repeater_sbp'); ?>" class="img-content"
                    data-aos="fade-up" data-aos-duration="1000">
                    <?php $imgSesionPortrait = get_sub_field('zdjecie_pionowe_wew_glowny_repeater_sbp'); ?>
                    <img src="<?php echo $imgSesionPortrait['sizes']['img-thumb-portrait']; ?>"
                        alt="<?php echo esc_attr($imgSesionPortrait['alt']); ?>">
                    <div class="text">
                        <h4><?php the_sub_field('nazwa_kategorii_sesji_wew_glowny_repeater_sbp'); ?></h4>
                        <h3><?php the_sub_field('tytul_sesji_wew_glowny_repeater_kopia_sbp'); ?></h3>
                        <span class="btn">sprawdź ofertę >></span>
                    </div>
                    <div class="overly">
                        <span class="line-1"></span>
                        <span class="line-2"></span>
                        <span class="line-3"></span>
                        <span class="line-4"></span>
                    </div>
                </a>

                <?php 
                endwhile;
                else :
                endif;
            ?>
                <?php if( get_field('przycisk_tekst_zdjecia_pionowe_glowny_repeater_sbp') ): ?>
                <div class="btn-wrapper btn-right" data-aos="fade-up" data-aos-duration="1000">
                    <a href="<?php the_field('przycisk_link_zdjecia_pionowe_glowny_repeater_sbp') ?>"
                        class="btn"><?php the_field('przycisk_tekst_zdjecia_pionowe_glowny_repeater_sbp') ?></a>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </section>


</main>



<?php get_footer(); ?>