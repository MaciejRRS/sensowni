var swiper = new Swiper('.mySwiper', {
    loop: false,
    slideToClickedSlide: true,
    autoplay: {
        slidesPerView: 1,
        delay: 2500,
        disableOnInteraction: false,
    // },
    autoplay: false,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    breakpoints: {
        320: {
            slidesPerView: 1,
            spaceBetween: 5,
        },

        576: {
            slidesPerView: 1,
            spaceBetween: 10,
        },
        692: {
            slidesPerView: 1,
            spaceBetween: 10
        },
        1200: {
            slidesPerView: 2,
            spaceBetween: 20
        },
        1920: {
            slidesPerView: 2,
            spaceBetween: 30
        }
    },
});


var swiper = new Swiper('.mySwiper-partners', {
    loop: true,
    auto: true,
    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
    },
    breakpoints: {
        320: {
            slidesPerView: 2,
            spaceBetween: 5,
        },

        576: {
            slidesPerView: 2,
            spaceBetween: 10,
        },
        692: {
            slidesPerView: 3,
            spaceBetween: 10
        },
        1200: {
            slidesPerView: 4,
            spaceBetween: 20
        },
        1920: {
            slidesPerView: 4,
            spaceBetween: 30
        }
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});
