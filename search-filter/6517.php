<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
} ?>


<div class="container">
    <div class="blog-posts">
        <?php if ( $query->have_posts() )
{
	?>

        <?php
	while ($query->have_posts())  
	{
		$query->the_post();
?>


        <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
        <div class="column-post">
            <h4><?php the_category(' / '); ?></h4>
            <a href="<?php echo get_permalink(); ?>">
                <div class="blog-title-area">
                    <h3 class="blog_post-title"><?php the_title(); ?></h3>
                </div>
                <div class="post-img">
                    <?php the_post_thumbnail( 'medium' ); ?>
                </div>

                <div class="blog_post-description">
                    <?php the_excerpt(); ?>
                </div>
                <span class="read-more"><?php the_field('blog_czytaj_wiecej_text_allPosts','options') ?> >></span>
            </a>
        </div>

        <?php }	?>
    </div>

</div>

<div class="container">
    <div class="pagination">

        <div class="nav-previous"><?php next_posts_link( '🠐 Cofnij', $query->max_num_pages ); ?></div>
        <div class="nav-next"><?php previous_posts_link( 'Dalej 🠒' ); ?></div>
        <?php
        /* example code for using the wp_pagenavi plugin */
        if (function_exists('wp_pagenavi'))
        {
            echo "<br />";
            wp_pagenavi( array( 'query' => $query ) );
        }
    ?>
    </div>
</div>
<?php
}
else
{
	echo "<div class='noFound'>Brak realizacji w wybranej kategorii, wybierz inną kategorię</div>";
}
?>